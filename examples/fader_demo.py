import logging
import time
import random
import sys

from launchcontrol import Color, DAWMode, LaunchpadX, Layout, FaderSpec, FaderOrientation, FaderType

log = logging.getLogger("fader_demo")

if "-v" in sys.argv[1:]:
    logging.basicConfig(level=logging.DEBUG)

pad = LaunchpadX(connect=False)
pad.connect(name="Launchpad X LPX DAW")
pad.set_daw_mode(DAWMode.DAW)
pad.set_layout(Layout.DAW_FADERS)
time.sleep(1.0)

try:
    for _ in range(3):
        faders = []
        log.debug("Setting up faders...")

        for i in range(8):
            fader = FaderSpec(i, FaderType.UNIPOLAR, i + 20, random.randint(1, 127))
            faders.append(fader)

        pad.setup_daw_faders(FaderOrientation.VERTICAL, *faders)

        for i in range(8):
            log.debug(f"Raising fader {i+1} (CC #{i + 20})...")

            for val in range(0, 128, 2):
                pad._send([0xB4, i + 20, val])
                time.sleep(0.002)

            pad._send([0xB4, i + 20, 127])

        for i in range(8):
            log.debug(f"Lowering fader {i+1} (CC #{i + 20})...")

            for val in range(127, -1, -2):
                pad._send([0xB4, i + 20, val])
                time.sleep(0.002)

            pad._send([0xB4, i + 20, 0])


except KeyboardInterrupt:
    log.debug("Cleaning up...")
    for i in range(8):
        pad._send([0xB4, i + 20, 0])

pad.set_programmer_mode(True)
time.sleep(1.0)
pad.set_daw_mode(DAWMode.STANDALONE)
pad.set_layout(Layout.NOTE_MODE)
pad.close()
