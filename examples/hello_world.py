import sys
import time

from launchcontrol import Color, LaunchpadX, LightingMode

try:
    text = sys.argv[1].encode("ascii", errors="ignore")
except:
    text = "Hello World!"

pad = LaunchpadX(connect=True)
speed = 5  # in cols per second

try:
    pad.set_programmer_mode(LightingMode.PROGRAMMER)
    pad.clear_pads()
    time.sleep(1.0)
    pad.scroll_text(text, speed=speed, color=Color.ORANGE)
    time.sleep(len(text) * 5 // abs(speed) + 1)  # approx. 5 cols on avg. per char
    pad.clear_text()
    time.sleep(1.0)
finally:
    pad.set_programmer_mode(LightingMode.LIVE)
    pad.close()
