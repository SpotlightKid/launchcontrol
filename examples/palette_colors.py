import time
from random import randint

from launchcontrol import ColorSpec, LaunchpadX, LightingMode, LightingType

pad = LaunchpadX(connect=True)


try:
    pad.set_programmer_mode(LightingMode.PROGRAMMER)
    pad.clear_pads()

    while True:
        colors = []
        i = 0
        for col in range(1, 9):
            for row in range(1, 9):
                colors.append(ColorSpec(LightingType.STATIC, row * 10 + col, i))
                i += 1

        pad.set_pad_colors(*colors)
        time.sleep(2.0)

        colors = []
        for col in range(1, 9):
            for row in range(1, 9):
                colors.append(ColorSpec(LightingType.STATIC, row * 10 + col, i))
                i += 1

        pad.set_pad_colors(*colors)
        time.sleep(2.0)
except KeyboardInterrupt:
    pass
finally:
    pad.clear_pads()
    pad.set_programmer_mode(LightingMode.LIVE)
    pad.close()
