import time
from random import randint

from launchcontrol import Color, ColorSpec, LaunchpadX, LightingMode, LightingType

pad = LaunchpadX(connect=True)


try:
    pad.set_programmer_mode(LightingMode.PROGRAMMER)
    pad.clear_pads()
    time.sleep(1.0)

    for _ in range(3):
        colors = []
        for col in range(1, 10):
            for row in range(1, 10):
                colors.append(
                    ColorSpec(
                        LightingType.RGB,
                        row * 10 + col,
                        (randint(0, 127), randint(0, 127), randint(0, 127)),
                    )
                )

        pad.set_pad_colors(*colors)
        time.sleep(3.0)

        colors = []
        for col in range(1, 10):
            for row in range(1, 10):
                colors.append(ColorSpec(LightingType.STATIC, row * 10 + col, randint(1, 127)))

        pad.set_pad_colors(*colors)
        time.sleep(3.0)

        colors = []
        for col in range(1, 10):
            for row in range(1, 10):
                colors.append(ColorSpec(LightingType.PULSING, row * 10 + col, randint(1, 127)))

        pad.set_pad_colors(*colors)
        time.sleep(3.0)

        colors = []
        for col in range(1, 10):
            for row in range(1, 10):
                colors.append(
                    ColorSpec(
                        LightingType.FLASHING, row * 10 + col, (randint(1, 127), randint(1, 127))
                    )
                )

        pad.set_pad_colors(*colors)
        time.sleep(3.0)

        colors = []
        for col in range(1, 10):
            for row in range(1, 10):
                colors.append(
                    ColorSpec(LightingType.FLASHING, row * 10 + col, (randint(1, 127), Color.BLANK))
                )

        pad.set_pad_colors(*colors)
        time.sleep(3.0)

        pad.clear_pads()
        time.sleep(1.0)
except KeyboardInterrupt:
    pass
finally:
    pad.clear_pads()
    time.sleep(1.0)
    pad.set_programmer_mode(LightingMode.LIVE)
    pad.close()
