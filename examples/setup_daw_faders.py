from rtmidi.midiconstants import *
from launchcontrol import Color, DAWMode, LaunchpadX, Layout, FaderSpec, FaderOrientation, FaderType

pad = LaunchpadX()
pad.set_daw_mode(DAWMode.DAW)
pad.set_layout(Layout.DAW_FADERS)

f_vol = FaderSpec(0, FaderType.UNIPOLAR, VOLUME, Color.BLUE)
f_pan = FaderSpec(1, FaderType.BIPOLAR, PAN, Color.ORANGE)

pad.setup_daw_faders(FaderOrientation.VERTICAL, f_vol, f_pan)

pad.close()
