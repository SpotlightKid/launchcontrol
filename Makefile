DESTDIR ?= /
INSTALL ?= install
PACKAGE = launchcontrol
PROJECT = launchcontrol
PACKAGE_US = launchcontrol
PREFIX ?= /usr/local
PYTHON ?= python3
TWINE ?= twine
PYVER = $(shell $(PYTHON) -c 'import sys;print("%s.%s" % sys.version_info[:2])')

.PHONY: all black build flake8 install install-user isort qa release uninstall

all:
	@echo 'make qa: run quality assurance tools (black, isort, flake8)'
	@echo 'make install: install package to $(DESTDIR:/=)$(PREFIX) (may need root privileges)'
	@echo 'make install-user: install package under home directory of current user'
	@echo 'make sdist: create source distribution archive in ./dist'
	@echo 'make wheel: create Python wheel package in ./dist'
	@echo 'make pypi-upload: create sdist and wheel and upload them to PyPI'

flake8:
	flake8 setup.py $(PACKAGE)

isort:
	isort setup.py $(PACKAGE)

black:
	black setup.py $(PACKAGE)

qa: black isort flake8

build:
	$(PYTHON) setup.py build

install: build
	$(PYTHON) setup.py install --skip-build --root=$(DESTDIR) --prefix=$(PREFIX) --optimize=1

install-user: build
	$(PYTHON) setup.py install --skip-build --optimize=1 --user

uninstall:
	rm -rf $(DESTDIR:/=)$(PREFIX)/lib/python$(PYVER)/site-packages/$(PACKAGE)
	rm -rf $(DESTDIR:/=)$(PREFIX)/lib/python$(PYVER)/site-packages/$(PACKAGE_US)-*.egg-info
	rm -f $(DESTDIR:/=)$(PREFIX)/bin/$(PROJECT)

sdist:
	$(PYTHON) setup.py sdist --formats=xztar

wheel:
	$(PYTHON) setup.py bdist_wheel

release: sdist wheel

pypi-upload: sdist wheel
	$(TWINE) upload --skip-existing dist/*.tar.gz dist/*.whl
