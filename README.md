# Launchcontrol

A Python library for controlling [Novation Launchpad and Launch Control] devices

**Note:** *This is an unreleased personal project in alpha stage. Though it may already be useful to others, I make no promises to ever finish it, or support it, or keep the API unchanged. At the moment, the library actually only provides support for the [Launchpad X].*


## Usage example

```py
from launchcontrol import Color, LaunchpadX

pad = LaunchpadX(connect=True)
print(pad.device_inquiry())
```
Output:

```py
{
    'device_id': 0,
    'manufacturer_id': (0, 32, 41),
    'device_family_id': (3, 1),
    'model_id': (0, 0),
    'app_version': (0, 3, 5, 1)
}
```

Let's show some scrolling text:

```py
import time

pad.set_programmer_mode()
pad.scroll_text("Hello World!", color=Color.ORANGE)
time.sleep(10.0)
pad.clear_text()
pad.close()
```

## Requirements

* Python 3.6+
* [python-rtmidi]

## License

**launchcontrol** was written and is copyrighted by Christopher Arndt, 2022.

It is distributed under the terms of the [MIT license]. **launchcontrol** is free
and open source software.

I am not affiliated with Novation, this software was neither created nor is it endorsed by Novation .

## References

* [Novation Launchpad X User Guide English](https://fael-downloads-prod.focusrite.com/customer/prod/s3fs-public/downloads/Launchpad%20X%20User%20Guide.pdf)
* [Novation Launchpad X Programmer's Reference Guide](https://fael-downloads-prod.focusrite.com/customer/prod/s3fs-public/downloads/Launchpad%20X%20-%20Programmers%20Reference%20Manual.pdf)


[novation launchpad and launch control]: https://novationmusic.com/en/launch
[launchpad x]: https://novationmusic.com/en/launch/launchpad-x
[python-rtmidi]: https://github.com/SpotlightKid/python-rtmidi
[mit license]: http://opensource.org/licenses/MIT