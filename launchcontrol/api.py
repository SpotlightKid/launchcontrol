import logging
import queue
from dataclasses import astuple, dataclass
from enum import IntEnum
from typing import Tuple, Union

import rtmidi
from rtmidi.midiconstants import END_OF_EXCLUSIVE, SYSTEM_EXCLUSIVE
from rtmidi.midiutil import open_midiinput, open_midioutput

log = logging.getLogger(__name__)

NOVATION_MANUFACTURER_ID = [0, 0x20, 0x29]
SYSEX_MSG_PREFIX = [SYSTEM_EXCLUSIVE] + NOVATION_MANUFACTURER_ID + [2, 0x0C]
LAYOUT_NAMES = {
    0: "Session",
    1: "Note mode",
    4: "Custom mode 1",
    5: "Custom mode 2",
    6: "Custom mode 3",
    7: "Custom mode 4",
    13: "DAW Faders",
    127: "Programmer Mode",
}

CMD_LAYOUT = 0x00
CMD_DAW_FADER = 0x01
CMD_LED_LIGHTING = 0x03
CMD_VELOCITY_CURVE = 0x04
CMD_SCROLL_TEXT = 0x07
CMD_BRIGHTNESS = 0x08
CMD_LED_SLEEP = 0x09
CMD_LED_FEEDBACK = 0x0A
CMD_AFTERTOUCH = 0x0B
CMD_FADER_VELOCITY_MODE = 0x0D
CMD_PROGRAMMER_MODE = 0x0E
CMD_DRUM_RACK_MODE = 0x0F
CMD_DAW_MODE = 0x10
CMD_CLEAR_DAW_STATE = 0x12
CMD_DRUM_RACK_POSITION = 0x13
CMD_SESSION_BUTTON_COLOR = 0x14
CMD_NOTE_MODE = 0x15
CMD_NOTE_MODE_CONFIG = 0x16
CMD_NOTE_MODE_ACTIVE_COLOR = 0x17


class AftertouchThreshold(IntEnum):
    LOW = 0
    MEDIUM = 1
    HIGH = 2


class AftertouchType(IntEnum):
    POLY = 0
    MONO = 1
    OFF = 2


class Color(IntEnum):
    BLANK = 0
    DARKGRAY = 1
    LIGHTGRAY = 2
    WHITE = 3
    RED = 5
    ORANGE = 9
    YELLOW = 13
    GREEN = 21
    CYAN = 37
    BLUE = 79
    MAGENTA = 95


class DAWMode(IntEnum):
    STANDALONE = 0
    DAW = 1


class DrumRackMode(IntEnum):
    NOTE_MODE = 0
    SIMPLE = 1
    INTELLIGENT = 2


class FaderOrientation(IntEnum):
    VERTICAL = 0
    HORIZONTAL = 1


class FaderType(IntEnum):
    BIPOLAR = 1
    UNIPOLAR = 0


class Layout(IntEnum):
    SESSION = 0
    NOTE_MODE = 1
    CUSTOM_MODE_1 = 4
    CUSTOM_MODE_2 = 5
    CUSTOM_MODE_3 = 6
    CUSTOM_MODE_4 = 7
    DAW_FADERS = 13
    PROGRAMMER_MODE = 127


class LightingMode(IntEnum):
    LIVE = 0
    PROGRAMMER = 1


class LightingType(IntEnum):
    STATIC = 0
    FLASHING = 1
    PULSING = 2
    RGB = 3


class NoteMode(IntEnum):
    CHROMATIC = 0
    SCALE = 1


class NoteScale(IntEnum):
    NATURAL_MINOR = 0
    MAJOR = 1
    DORIAN = 2
    PHRYGIAN = 3
    MIXOLYDIAN = 4
    MELODIC_MINOR = 5
    HARMONIC_MINOR = 6
    BEBOP_DORIAN = 7
    BLUES = 8
    MINOR_PENTATONIC = 9
    HUNGARIAN_MINOR = 10
    UKRANIAN_DORIAN = 11
    MARVA = 12
    TODI = 13
    WHOLE_TONE = 14
    HIRAJOSHI = 15


class VelocityCurve(IntEnum):
    SOFT = 0
    MEDIUM = 1
    HARD = 2
    FIXED = 3


@dataclass(init=True)
class ColorSpec:
    type: LightingType
    index: int
    data: Union[int, Tuple[int, int], Tuple[int, int, int]]

    def __post_init__(self):
        if self.index % 10 == 0 or self.index // 10 > 9 or self.index // 10 < 1:
            raise ValueError("Color index must be 11..19, 21..29, 31..39, ..., 91..99.")

    def astuple(self):
        if self.type == LightingType.RGB:
            return (self.type, self.index, self.data[0], self.data[1], self.data[2])
        elif self.type == LightingType.FLASHING:
            return (self.type, self.index, self.data[0], self.data[1])
        else:
            return (self.type, self.index, self.data)


@dataclass(init=True)
class FaderSpec:
    index: int
    type: FaderType
    cc: int
    color: Color

    def __post_init__(self):
        if self.index < 0 or self.index > 7:
            raise ValueError("Fader index must be 0..7")

    def astuple(self):
        return astuple(self)


def make_sysex_msg(command, *data):
    _data = []
    for item in data:
        if isinstance(item, (tuple, list)):
            _data.extend(list(item))
        else:
            _data.append(item)

    return SYSEX_MSG_PREFIX + [command & 0x7F] + _data + [END_OF_EXCLUSIVE]


def match_event(event, spec):
    data = {}
    field_lengths = [(x[1] if isinstance(x, (list, tuple)) else 1) for x in spec]

    if "*" not in field_lengths:
        expected_length = sum(field_lengths)

        if len(event) < expected_length:
            return False

    offset = 0
    for item in spec:
        if isinstance(item, str):
            data[item] = event[offset]
            offset += 1
        elif isinstance(item, (list, tuple)):
            name, length = item

            if length == "*":
                data[name] = tuple(event[offset:-1])
                offset = len(event) - 1
            else:
                data[name] = tuple(event[offset : offset + length])  # noqa: E203
                offset += length
        elif item != event[offset]:
            return False
        else:
            offset += 1

    return data


class LaunchpadX:
    name = "Launchpad X LPX"
    columns = 9
    rows = 9

    def __init__(self, connect=True, interactive=False, daw=False, timeout=3.0):
        self.midiin = self.midiin_name = None
        self.midiout = self.midiout_name = None
        self.timeout = timeout

        if connect:
            self.connect(interactive=interactive, daw=daw)

    def connect(self, name=None, interactive=False, daw=False):
        if not name:
            name = self.name + (" DAW" if daw else " MIDI")

        self.midiin, self.midiin_name = open_midiinput(name, interactive=interactive)
        log.debug("Connected to MIDI input '%s'.", self.midiin_name)
        self.midiout, self.midiout_name = open_midioutput(name, interactive=interactive)
        log.debug("Connected to MIDI output '%s'.", self.midiout_name)

        self._inq = queue.Queue()
        self.midiin.ignore_types(sysex=False)
        self.midiin.set_callback(self._midiin_callback)

    def close(self):
        if self.midiin:
            self.midiin.close_port()
            self.midiin.delete()
            self.midiin = self.midiin_name = None

        if self.midiout:
            self.midiout.close_port()
            self.midiout.delete()
            self.midiout = self.midiout_name = None

    # ---------------- Universal Device Indentity Request / Reply: --------

    def device_inquiry(self, device=0x7F):
        self._flush()
        self._send([0xF0, 0x7E, device, 6, 1, 0xF7])
        return self._expect(
            [
                0xF0,
                0x7E,
                "device_id",
                6,
                2,
                ("manufacturer_id", 3),
                ("device_family_id", 2),
                ("model_id", 2),
                ("app_version", 4),
                0xF7,
            ]
        )

    # ---------------- Layouts and Modes: ---------------------------------

    def get_layout(self):
        self._flush()
        self._send(make_sysex_msg(CMD_LAYOUT))
        resp = self._expect_command_response(CMD_LAYOUT, "layout")
        return resp["layout"]

    def set_layout(self, layout=Layout.NOTE_MODE):
        self._send(make_sysex_msg(CMD_LAYOUT, layout & 0x7F))

    def get_note_mode(self):
        self._flush()
        self._send(make_sysex_msg(CMD_NOTE_MODE))
        resp = self._expect_command_response(CMD_NOTE_MODE, "mode")
        return resp["mode"]

    def set_note_mode(self, mode=NoteMode.CHROMATIC):
        self._send(make_sysex_msg(CMD_NOTE_MODE, 1 if mode else 0))

    def get_programmer_mode(self):
        self._flush()
        self._send(make_sysex_msg(CMD_PROGRAMMER_MODE))
        resp = self._expect_command_response(CMD_PROGRAMMER_MODE, "mode")
        return resp["mode"]

    def set_programmer_mode(self, mode=LightingMode.PROGRAMMER):
        self._send(make_sysex_msg(CMD_PROGRAMMER_MODE, 1 if mode else 0))

    def get_daw_mode(self):
        self._flush()
        self._send(make_sysex_msg(CMD_DAW_MODE))
        resp = self._expect_command_response(CMD_DAW_MODE, "mode")
        return resp["mode"]

    def set_daw_mode(self, mode=DAWMode.DAW):
        self._send(make_sysex_msg(CMD_DAW_MODE, 1 if mode else 0))

    def clear_daw_state(self, session=True, drumrack=True, controlchange=True):
        self._send(
            make_sysex_msg(
                CMD_CLEAR_DAW_STATE,
                1 if session else 0,
                1 if drumrack else 0,
                1 if controlchange else 0,
            )
        )

    def get_drum_rack_mode(self):
        self._flush()
        self._send(make_sysex_msg(CMD_DRUM_RACK_MODE))
        resp = self._expect_command_response(CMD_DRUM_RACK_MODE, "mode")
        return resp["mode"]

    def set_drum_rack_mode(self, mode=DrumRackMode.INTELLIGENT):
        self._send(make_sysex_msg(CMD_DRUM_RACK_MODE, mode & 0b11))

    # ---------------- Device State: --------------------------------------

    def get_info(self):
        info = self.device_inquiry()
        info["daw_mode"] = DAWMode(self.get_daw_mode())
        info["drum_rack_mode"] = DrumRackMode(self.get_drum_rack_mode())
        info["layout"] = Layout(self.get_layout())
        info["note_mode"] = NoteMode(self.get_note_mode())
        info["programmer_mode"] = LightingMode(self.get_programmer_mode())
        return info

    # ---------------- DAW Faders: ----------------------------------------

    def setup_daw_faders(self, orientation=FaderOrientation.HORIZONTAL, *faders):
        if not faders:
            raise ValueError("At least one Fader instance must be passed.")

        if len(faders) > 8:
            raise ValueError("Only up to eight faders are supported.")

        self._send(
            make_sysex_msg(
                CMD_DAW_FADER,
                0,
                orientation & 0b1,
                *[(f.astuple() if isinstance(f, FaderSpec) else f) for f in faders],
            )
        )

    # ---------------- DAW Drum Rack: -------------------------------------

    def get_drum_rack_position(self):
        self._flush()
        self._send(make_sysex_msg(CMD_DRUM_RACK_POSITION))
        resp = self._expect_command_response(CMD_DRUM_RACK_POSITION, "position")
        return resp["position"]

    def set_drum_rack_position(self, position):
        self._send(make_sysex_msg(CMD_DRUM_RACK_POSITION, max(0, min(position, 64))))

    def set_drum_pad_color(self, btn, color):
        self._send([0x98, btn & 0x7F, color & 0x7F])

    def set_drum_pad_pulsing_color(self, btn, color):
        self._send([0x9A, btn & 0x7F, color & 0x7F])

    def set_drum_pad_flashing_color(self, btn, color, coloroff=None):
        if coloroff:
            self.set_pad_color(btn, coloroff, use_cc=use_cc)

        self._send([0x99, btn & 0x7F, color & 0x7F])

    # ---------------- Session Button Colors: ------------------------------------

    def get_session_button_color(self):
        self._flush()
        self._send(make_sysex_msg(CMD_SESSION_BUTTON_COLOR))
        return self._expect_command_response(CMD_SESSION_BUTTON_COLOR, "active", "inactive")

    def set_session_button_color(self, active=Color.BLANK, inactive=Color.BLANK):
        self._send(make_sysex_msg(CMD_SESSION_BUTTON_COLOR, active & 0x7F, inactive & 0x7F))

    # ---------------- Note Mode Active Color: ----------------------------

    def get_note_mode_active_color(self):
        self._flush()
        self._send(make_sysex_msg(CMD_NOTE_MODE_ACTIVE_COLOR))
        resp = self._expect_command_response(CMD_NOTE_MODE_ACTIVE_COLOR, "color")
        return resp["color"]

    def set_note_mode_active_color(self, color=Color.GREEN):
        self._send(make_sysex_msg(CMD_NOTE_MODE_ACTIVE_COLOR, color & 0x7F))

    # ---------------- Note Mode Pads and Button Colors: ------------------

    def set_pad_color(self, btn, color, use_cc=True):
        self._send([0xB0 if use_cc else 0x90, btn & 0x7F, color & 0x7F])

    def set_pad_pulsing_color(self, btn, color, use_cc=True):
        self._send([0xB2 if use_cc else 0x92, btn & 0x7F, color & 0x7F])

    def set_pad_flashing_color(self, btn, color, coloroff=None, use_cc=True):
        if coloroff:
            self.set_pad_color(btn, coloroff, use_cc=use_cc)

        self._send([0xB1 if use_cc else 0x91, btn & 0x7F, color & 0x7F])

    def set_pad_colors(self, *colorspecs):
        if not colorspecs:
            raise ValueError("At least one ColorSpec instance must be passed.")

        if len(colorspecs) > 81:
            raise ValueError("Only up to 81 button colors are supported.")

        self._send(
            make_sysex_msg(
                CMD_LED_LIGHTING,
                *[(c.astuple() if isinstance(c, ColorSpec) else c) for c in colorspecs],
            )
        )

    def clear_pads(self):
        self.set_pad_colors(
            *(
                (LightingType.STATIC, r * 10 + c, Color.BLANK)
                for r in range(1, self.rows + 1)
                for c in range(1, self.columns + 1)
            )
        )

    def show_image(self, data):
        colors = []
        col = 1
        row = self.rows

        for color in data:
            if color & 0x80:
                type = LightingType.PULSING
            else:
                type = LightingType.STATIC

            colors.append(ColorSpec(type, row * 10 + col, color & 0x7F))
            col += 1

            if col > self.columns:
                row -= 1
                col = 1

        self.set_pad_colors(*colors)

    # ---------------- Device Configuration: ------------------------------

    def get_brightness(self):
        self._flush()
        self._send(make_sysex_msg(CMD_BRIGHTNESS))
        resp = self._expect_command_response(CMD_BRIGHTNESS, "brightness")
        return resp["brightness"]

    def set_brightness(self, brightness):
        self._send(make_sysex_msg(CMD_BRIGHTNESS, brightness & 0x7F))

    def get_led_feedback(self):
        self._flush()
        self._send(make_sysex_msg(CMD_LED_FEEDBACK))
        return self._expect_command_response(CMD_LED_FEEDBACK, "internal", "external")

    def set_led_feedback(self, internal=True, external=True):
        self._send(make_sysex_msg(CMD_LED_FEEDBACK, 1 if internal else 0, 1 if external else 0))

    def get_sleep_mode(self):
        self._flush()
        self._send(make_sysex_msg(CMD_LED_SLEEP))
        resp = self._expect_command_response(CMD_LED_SLEEP, "mode")
        return resp["mode"]

    def set_sleep_mode(self, mode=True):
        self._send(make_sysex_msg(CMD_LED_SLEEP, 1 if mode else 0))

    def get_velocity_curve(self):
        self._flush()
        self._send(make_sysex_msg(CMD_VELOCITY_CURVE))
        return self._expect_command_response(CMD_VELOCITY_CURVE, "curve", "fixed")

    def set_velocity_curve(self, curve=VelocityCurve.MEDIUM, fixed=0x7F):
        self._send(make_sysex_msg(CMD_VELOCITY_CURVE, curve & 0b11, fixed & 0x7F))

    def get_aftertouch(self):
        self._flush()
        self._send(make_sysex_msg(CMD_AFTERTOUCH))
        return self._expect_command_response(CMD_AFTERTOUCH, "type", "threshold")

    def set_aftertouch(self, type=AftertouchType.POLY, threshold=AftertouchThreshold.MEDIUM):
        self._send(make_sysex_msg(CMD_AFTERTOUCH, type & 0b11, threshold & 0b11))

    def get_fader_velocity_mode(self):
        self._flush()
        self._send(make_sysex_msg(CMD_FADER_VELOCITY_MODE))
        resp = self._expect_command_response(CMD_FADER_VELOCITY_MODE, "mode")
        return resp["mode"]

    def set_fader_velocity_mode(self, mode=True):
        self._send(make_sysex_msg(CMD_FADER_VELOCITY_MODE, 1 if mode else 0))

    # ---------------- Note Mode: -----------------------------------------

    def get_note_mode_config(self, mode=None):
        self._flush()

        if mode is None:
            self._send(make_sysex_msg(CMD_NOTE_MODE_CONFIG))
        else:
            self._send(make_sysex_msg(CMD_NOTE_MODE_CONFIG, mode & 0xB1))

        return self._expect_command_response(
            CMD_NOTE_MODE_CONFIG,
            "mode",
            "octave",
            "transposition",
            "channel",
            "width",
            "scale_type",
            ("scale", "*"),
        )

    def set_note_mode_config(
        self,
        mode=NoteMode.CHROMATIC,
        octave=0,
        transposition=0,
        channel=0,
        width=None,
        scale=None,
    ):
        if scale:
            if isinstance(scale, (tuple, list)) and len(scale) != 11:
                raise ValueError("Custom scale must be passed as a sequence of 11 ints/booleans.")

            if width is None:
                width = 7 if mode else 8

            self._send(
                make_sysex_msg(
                    CMD_NOTE_MODE_CONFIG,
                    mode & 0b1,
                    octave & 0x7F,
                    transposition & 0x7F,
                    channel & 0x7F,
                    width & 0x7F,
                    0 if isinstance(scale, int) else 1,
                    scale & 0x7F
                    if isinstance(scale, int)
                    else [1 if note else 0 for note in scale],
                )
            )
        else:
            self._send(
                make_sysex_msg(
                    CMD_NOTE_MODE_CONFIG,
                    mode & 0b1,
                    octave & 0x7F,
                    transposition & 0x7F,
                )
            )

    # ---------------- Text Display: --------------------------------------

    def scroll_text(self, text, color=Color.WHITE, speed=5, loop=True):
        if isinstance(text, str):
            text = text.encode("ascii")

        self._send(
            make_sysex_msg(
                CMD_SCROLL_TEXT,
                1 if loop else 0,
                speed & 0x7F,
                (0, color & 0x7F)
                if isinstance(color, int)
                else (1, color[0] & 0x7F, color[1] & 0x7F, color[2] & 0x7F),
                tuple(text),
            )
        )

    def configure_text(self, color=Color.WHITE, speed=5, loop=True):
        self._send(
            make_sysex_msg(
                CMD_SCROLL_TEXT,
                1 if loop else 0,
                speed & 0x7F,
                (0, color & 0x7F)
                if isinstance(color, int)
                else (1, color[0] & 0x7F, color[1] & 0x7F, color[2] & 0x7F),
            )
        )

    def clear_text(self):
        self._send(make_sysex_msg(CMD_SCROLL_TEXT))

    # ---------------- Internal methods: ----------------------------------

    def _midiin_callback(self, event, data):
        msg, dt = event
        log.debug("RECV: %s", " ".join(f"{x:02X}" for x in msg))
        self._inq.put(msg)

    def _flush(self):
        """Flush input queue events."""
        try:
            while True:
                self._inq.get_nowait()
        except queue.Empty:
            pass

    def _send(self, msg):
        log.debug("SEND: %s", " ".join(f"{x:02X}" for x in msg))

        if self.midiout:
            self.midiout.send_message(msg)
        else:
            log.warning("MIDI output not opened.")

    def _expect_command_response(self, command, *spec):
        return self._expect(SYSEX_MSG_PREFIX + [command] + list(spec))

    def _expect(self, match, *, timeout=None):
        if not self.midiin:
            raise IOError("MIDI input not opened.")

        while True:
            try:
                event = self._inq.get(timeout=timeout if timeout is not None else self.timeout)
            except queue.Empty:
                raise IOError(
                    f"Timout waiting for expected response on MIDI input '{self.midiin_name}'."
                )

            if event:
                if (data := match_event(event, match)) is not False:
                    return data


def _main(args=None):
    if "-v" in args:
        logging.basicConfig(level=logging.DEBUG)
        args.remove("-v")

    pad = LaunchpadX(connect=False)

    try:
        pad.connect(args[0] if args else None)
    except rtmidi.InvalidPortError:
        return "Launchpad MIDI port not found. Is your Launchpad connected?"

    info = pad.get_info()
    print(
        (
            "Manufacturer ID: {manufacturer_id}\n"
            "Device family ID: {device_family_id}\n"
            "Model ID: {model_id}\n"
            "Device ID: {device_id}\n"
            "App version: {app_version}\n"
            "Layout: {layout!r}\n"
            "Note mode: {note_mode!r}\n"
            "DAW Mode: {daw_mode!r}\n"
            "Drum Rack Mode: {drum_rack_mode!r}\n"
            "Programmer Mode: {programmer_mode!r}"
        ).format(**info)
    )

    pad.close()


if __name__ == "__main__":
    import sys

    sys.exit(_main(sys.argv[1:]) or 0)
